package u05lab.code

object Kind extends Enumeration {
  val Retired, Failed, Succeeded = Value
}

sealed trait ExamResult {
  def kind: Kind.Value

  def evaluation: Option[Int]

  def cumLaude: Boolean
}

private case class ExamResultImpl( kind: Kind.Value,
                                   private val _evaluation: Option[Int] = Option.empty,
                                   cumLaude: Boolean = false ) extends ExamResult {

  if (_evaluation.isDefined && (_evaluation.get < 18 || _evaluation.get > 30)) throw new IllegalArgumentException

  override def evaluation: Option[Int] = _evaluation

  override def toString: String = kind match {
    case Kind.Succeeded => kind.toString + "(" + _evaluation.get + (if (cumLaude) "L)" else ")")
    case Kind.Failed | Kind.Retired => kind.toString
  }
}

object ExamResultFactory {
  def failed(): ExamResult = ExamResultImpl(Kind.Failed)

  def retired(): ExamResult = ExamResultImpl(Kind.Retired)

  def succeededCumLaude(): ExamResult =  ExamResultImpl(Kind.Succeeded, Some(30), cumLaude = true)

  def succeeded(evaluation: Int): ExamResult = ExamResultImpl(Kind.Succeeded, Some(evaluation))
}

sealed trait ExamsManager {
  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, result: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationsMapFromCall(call: String): Map[String, Int]

  def getResultsMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]
}

object ExamsManager{
  def apply(): ExamsManager = ExamsManagerImpl()

  case class ExamsManagerImpl() extends ExamsManager {
    private var calls: collection.mutable.Map[String, collection.mutable.Map[String, ExamResult]] = collection.mutable.Map.empty

    override def createNewCall(call: String): Unit = if (calls.contains(call)) {
      throw new IllegalArgumentException
    } else {
      calls += (call -> collection.mutable.Map.empty)
    }

    override def addStudentResult(call: String, student: String, result: ExamResult): Unit = if (calls(call).contains(student)) {
      throw new IllegalArgumentException
    } else {
      calls(call) += (student -> result)
    }

    override def getAllStudentsFromCall(call: String): Set[String] = calls(call).keySet.toSet

    override def getEvaluationsMapFromCall(call: String): Map[String, Int] = calls(call).filter(s => s._2.evaluation.isDefined).mapValues(v => v.evaluation.get).toMap

    override def getResultsMapFromStudent(student: String): Map[String, String] = calls.filter(call => call._2.keySet.contains(student)).mapValues(stud => stud(student).toString).toMap

    override def getBestResultFromStudent(student: String): Option[Int] = calls.filter(call => call._2.keySet.contains(student)).mapValues(stud => stud(student).evaluation).values.max
  }
}
