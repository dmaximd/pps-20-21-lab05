package u05lab.code

import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {
  import PerformanceUtils._
  import scala.collection._

  val minVal = 0
  val maxVal = 1000000
  val lastIndex = 999999
  val valToAdd = -1

  /* Linear sequences: List, ListBuffer */
  var immutableList: immutable.List[Int] = immutable.List.range(minVal, maxVal)
  println("List:")
  measure(" Creation")(immutable.List.range(minVal, maxVal))
  measure(" Size")(immutableList.size)
  measure(" Get last")(immutableList.last)
  measure(" Append")(immutableList = immutableList :+ valToAdd)
  measure(" Delete")(immutableList.dropRight(1))
  println()

  val mutableList: mutable.ListBuffer[Int] = mutable.ListBuffer.range(minVal, maxVal)
  println("ListBuffer:")
  measure(" Creation")(mutable.ListBuffer.range(minVal, maxVal))
  measure(" Size")(mutableList.size)
  measure(" Get last")(mutableList.last)
  measure(" Append")(mutableList.append(valToAdd))
  measure(" Delete")(mutableList.dropRight(1))
  println()

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  var vector: Vector[Int] = Vector.range(minVal, maxVal)
  println("Vector:")
  measure(" Creation")(Vector.range(minVal, maxVal))
  measure(" Size")(vector.size)
  measure(" Get last")(vector.last)
  measure(" Append")(vector = vector :+ valToAdd)
  measure(" Delete")(vector.dropRight(1))
  println()

  val array: Array[Int] = Array.range(minVal, maxVal)
  println("Array:")
  measure(" Creation")(Array.range(minVal, maxVal))
  measure(" Size")(array.length)
  measure(" Get last")(array.last)
  measure(" Set last")(array(lastIndex) = valToAdd)
  measure(" Delete")(array.dropRight(1))
  println()

  val arrayBuffer: mutable.ArrayBuffer[Int] = mutable.ArrayBuffer.range(minVal, maxVal)
  println("ArrayBuffer:")
  measure(" Creation")(mutable.ArrayBuffer.range(minVal, maxVal))
  measure(" Size")(arrayBuffer.size)
  measure(" Get last")(arrayBuffer.last)
  measure(" Append")(arrayBuffer.append(valToAdd))
  measure(" Delete")(arrayBuffer.dropRight(1))
  println()

  /* Sets */
  var set: Set[Int] = Set(minVal to maxVal:_*)
  println("Immutable Set:")
  measure(" Creation")(Set(minVal to maxVal:_*))
  measure(" Size")(set.size)
  measure(" Get last")(set.last)
  measure(" Append")(set += valToAdd)
  measure(" Delete")(set.dropRight(1))
  println()

  val mutableSet: mutable.Set[Int] = mutable.Set(minVal to maxVal: _*)
  println("Mutable Set:")
  measure(" Creation")(mutable.Set(minVal to maxVal:_*))
  measure(" Size")(mutableSet.size)
  measure(" Get last")(mutableSet.last)
  measure(" Append")(mutableSet += valToAdd)
  measure(" Delete")(mutableSet.dropRight(1))
  println()

  /* Maps */
  var map: Map[Int, Int] = Map((minVal to maxVal).zipWithIndex:_*)
  println("Immutable Map:")
  measure(" Creation")(Map((minVal to maxVal).zipWithIndex:_*))
  measure(" Size")(map.size)
  measure(" Get last")(map.last)
  measure(" Append")(map = map + (valToAdd -> valToAdd))
  measure(" Delete")(map.dropRight(1))
  println()

  val mutableMap: mutable.Map[Int, Int] = mutable.Map((minVal to maxVal).zipWithIndex:_*)
  println("Mutable Map:")
  measure(" Creation")(mutable.Map((minVal to maxVal).zipWithIndex:_*))
  measure(" Size")(mutableMap.size)
  measure(" Get last")(mutableMap.last)
  measure(" Append")(mutableMap.put(valToAdd, valToAdd))
  measure(" Delete")(mutableMap.dropRight(1))
  println()

  /* Comparison */
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}