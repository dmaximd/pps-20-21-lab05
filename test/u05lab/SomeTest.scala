package u05lab

import u05lab.code._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class SomeTest {

  val l: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  val single: List[Int] = List(1)
  val empty: List[Int] = List.nil

  @Test
  def testZipRight(): Unit ={
    val l = List("a", "b", "c")

    assertEquals(List.nil, List.nil.zipRight)
    assertEquals(List(("a", 0), ("b", 1), ("c", 2)), l.zipRight)
  }

  @Test
  def testPartition(): Unit ={
    assertEquals((List.nil, List.nil), empty.partition(_ > 5))
    assertEquals((List(2, 4, 6, 8), List(1, 3, 5, 7)), l.partition(_ % 2 == 0))
    assertEquals((l, List.nil), l.partition(_ < 10))
  }

  @Test
  def testSpan(): Unit ={
    assertEquals((List.nil, List.nil), empty.span(_ > 5))
    assertEquals((List(1, 2, 3, 4), List(5, 6, 7, 8)), l.span(_ < 5))
    assertEquals((l, List.nil), l.span(_ < 10))
  }

  @Test
  def testReduce(): Unit ={
    val list = List(1, 1, 1, 1, 1)
    val s = List("a", "b", "c")

    assertThrows(classOf[UnsupportedOperationException], () => empty.reduce(_ + _))
    assertEquals(5, list.reduce(_ + _))
    assertEquals(1, single.reduce(_ + _))
    assertEquals("abc", s.reduce(_ + _))
  }

  @Test
  def testTakeRight(): Unit ={
    assertEquals(empty, empty.takeRight(1))
    assertEquals(List(6, 7, 8), l.takeRight(3))
    assertEquals(single, single.takeRight(2))
  }

  @Test
  def testCollect(): Unit ={
    assertEquals(empty, empty.collect{ case x if x % 2 == 0 => x * 2 })
    assertEquals(List(4, 8, 12, 16), l.collect{ case x if x % 2 == 0 => x * 2 })
  }
}