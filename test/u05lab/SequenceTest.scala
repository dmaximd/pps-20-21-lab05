package u05lab

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class SequenceTest {

  def sequence[A](a: List[Option[A]]): Option[List[A]] = {
    val acc: Option[List[A]] = Some(List.empty)
    a.foldRight(acc)((el, res) => if (el.isDefined && res.isDefined) {
      Some(el.get :: res.get)
    } else {
      None
    })
  }

  @Test
  def TestSequence(): Unit ={
    val someList: List[Option[Int]] = List(Some(1), Some(2), Some(3))
    val noneList: List[Option[Int]] = List(Some(1), None, Some(3))
    assertEquals(Some(List(1, 2, 3)), sequence(someList))
    assertEquals(None, sequence(noneList))
  }
}
