package u05lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ExamsManagerTest {

  import u05lab.code.Kind
  import u05lab.code.ExamResultFactory
  import u05lab.code.ExamsManager

  private val erf = ExamResultFactory
  private val em = ExamsManager()

  // verifica base di ExamResultFactory
  @Test
  def testExamResultsBasicBehaviour(): Unit = {
    // esame fallito, non c'è voto
    assertEquals(Kind.Failed, erf.failed().kind)
    assertFalse(erf.failed().evaluation.isDefined)
    assertFalse(erf.failed().cumLaude)
    assertEquals("Failed", erf.failed().toString)

    // lo studente si è ritirato, non c'è voto
    assertEquals(Kind.Retired, erf.retired().kind)
    assertFalse(erf.retired().evaluation.isDefined)
    assertFalse(erf.retired().cumLaude)
    assertEquals("Retired", erf.retired().toString)

    // 30L
    assertEquals(Kind.Succeeded, erf.succeededCumLaude().kind)
    assertEquals(erf.succeededCumLaude().evaluation, Some(30))
    assertTrue(erf.succeededCumLaude().cumLaude)
    assertEquals("Succeeded(30L)", erf.succeededCumLaude().toString)

    // esame superato, ma non con lode
    assertEquals(Kind.Succeeded, erf.succeeded(28).kind)
    assertEquals(erf.succeeded(28).evaluation, Some(28))
    assertFalse(erf.succeeded(28).cumLaude)
    assertEquals("Succeeded(28)", erf.succeeded(28).toString)
  }

  // verifica eccezione in ExamResultFactory
  @Test
  def optionalTestEvaluationCantBeGreaterThan30(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(32))
  }
  // verifica eccezione in ExamResultFactory
  @Test
  def optionalTestEvaluationCantBeSmallerThan18(): Unit = {
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(17))
  }

  // metodo di creazione di una situazione di risultati in 3 appelli// metodo di creazione di una situazione di risultati in 3 appelli
  private def prepareExams(): Unit = {
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")

    em.addStudentResult("gennaio", "rossi", erf.failed()) // rossi -> fallito
    em.addStudentResult("gennaio", "bianchi", erf.retired()) // bianchi -> ritirato
    em.addStudentResult("gennaio", "verdi", erf.succeeded(28)) // verdi -> 28
    em.addStudentResult("gennaio", "neri", erf.succeededCumLaude()) // neri -> 30L

    em.addStudentResult("febbraio", "rossi", erf.failed()) // etc..
    em.addStudentResult("febbraio", "bianchi", erf.succeeded(20))
    em.addStudentResult("febbraio", "verdi", erf.succeeded(30))

    em.addStudentResult("marzo", "rossi", erf.succeeded(25))
    em.addStudentResult("marzo", "bianchi", erf.succeeded(25))
    em.addStudentResult("marzo", "viola", erf.failed())
  }

  // verifica base della parte obbligatoria di ExamManager// verifica base della parte obbligatoria di ExamManager
  @Test
  def testExamsManagement(): Unit = {
    this.prepareExams()

    // partecipanti agli appelli di gennaio e marzo
    assertEquals(em.getAllStudentsFromCall("gennaio"), Set("rossi", "bianchi", "verdi", "neri"))
    assertEquals(em.getAllStudentsFromCall("marzo"), Set("rossi", "bianchi", "viola"))

    // promossi di gennaio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("gennaio").size)
    assertEquals(28, em.getEvaluationsMapFromCall("gennaio")("verdi"))
    assertEquals(30, em.getEvaluationsMapFromCall("gennaio")("neri"))

    // promossi di febbraio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("febbraio").size)
    assertEquals(20, em.getEvaluationsMapFromCall("febbraio")("bianchi"))
    assertEquals(30, em.getEvaluationsMapFromCall("febbraio")("verdi"))

    // tutti i risultati di rossi (attenzione ai toString!!)
    assertEquals(3, em.getResultsMapFromStudent("rossi").size)
    assertEquals("Failed", em.getResultsMapFromStudent("rossi")("gennaio"))
    assertEquals("Failed", em.getResultsMapFromStudent("rossi")("febbraio"))
    assertEquals("Succeeded(25)", em.getResultsMapFromStudent("rossi")("marzo"))
    // tutti i risultati di bianchi
    assertEquals(3, em.getResultsMapFromStudent("bianchi").size)
    assertEquals("Retired", em.getResultsMapFromStudent("bianchi")("gennaio"))
    assertEquals("Succeeded(20)", em.getResultsMapFromStudent("bianchi")("febbraio"))
    assertEquals("Succeeded(25)", em.getResultsMapFromStudent("bianchi")("marzo"))
    // tutti i risultati di neri
    assertEquals(1, em.getResultsMapFromStudent("neri").size)
    assertEquals("Succeeded(30L)", em.getResultsMapFromStudent("neri")("gennaio"))
  }

  import java.util.Optional

  // verifica del metodo ExamManager.getBestResultFromStudent// verifica del metodo ExamManager.getBestResultFromStudent
  @Test
  def optionalTestExamsManagement(): Unit = {
    this.prepareExams()

    // miglior voto acquisito da ogni studente, o vuoto..
    assertEquals(Some(25), em.getBestResultFromStudent("rossi"))
    assertEquals(Some(25), em.getBestResultFromStudent("bianchi"))
    assertEquals(Some(30), em.getBestResultFromStudent("neri"))
    assertEquals(None, em.getBestResultFromStudent("viola"))
  }


  @Test
  def optionalTestCantCreateACallTwice(): Unit = {
    this.prepareExams()

    assertThrows(classOf[IllegalArgumentException], () => em.createNewCall("marzo"))
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice(): Unit = {
    this.prepareExams()

    assertThrows(classOf[IllegalArgumentException], () => em.addStudentResult("gennaio", "verdi", erf.failed()))
  }
}
